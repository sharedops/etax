﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RRA.ETax.Utilities.Services;


namespace RRA.Etax.Tests
{
    [TestClass]
    public class EmailTests
    {
        [TestMethod]
        public void SendEmailTest()
        {
            EmailUtility utility = new EmailUtility();
            string ToAddress = "manikanta.mundru@gmail.com";
            string Body = "Hello This is Test";
            string Subject = "MS Test Email" + DateTime.Now;
            bool IsWorking = utility.SendEmail(ToAddress, Body, Subject);
            Assert.AreEqual(IsWorking, true);
        }
    }
}
