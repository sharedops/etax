﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RRA.ETax.Repository.Models
{
    public class DbConnection
    {
        public string ConnectionString { get; set; }
        public string StoredProcedure { get; set; }
        public object Parameters { get; set; }
    }
}
