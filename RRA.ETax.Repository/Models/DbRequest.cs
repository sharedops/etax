﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RRA.ETax.Repository.Models
{
    public class DbRequest<T> where T: new()
    {
        public DbConnection DbConnection { get; set; } = new DbConnection();
        public T item { get; set; }
    }
}
