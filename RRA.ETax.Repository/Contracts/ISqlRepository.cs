﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RRA.ETax.Repository.Models;

namespace RRA.ETax.Repository.Contracts
{
    public interface ISqlRepository
    {
        Task<T> QueryOne<T>(DbConnection connection) where T : new();
        Task<IList<T>> QueryList<T>(DbConnection connection) where T : new();
        Task<int> Execute(DbConnection connection);

    }
}
