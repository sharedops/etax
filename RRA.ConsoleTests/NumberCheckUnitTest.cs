﻿using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace RRA.ConsoleTests
{
    [TestClass]
    public class NumberCheckUnitTest
    {

        [TestMethod]
        public void IntDataTypeCheckTest()
        {
            int Number;
            string input = "5";
            bool isInteger = int.TryParse(input, out Number);
            Assert.AreEqual(isInteger, true);
        }

        [TestMethod]
        public void StringDataTypeCheckTest()
        {
            int Number;
            bool isInteger = int.TryParse("I am String", out Number);
            Assert.AreEqual(isInteger, false);
        }

        [TestMethod]
        public void LessNumberCheck()
        {
            int output = NumberUtility.CheckNumber(4, 5);
            Assert.AreEqual(output, 1);
        }

        [TestMethod]
        public void EqualNumberCheck()
        {
            int output = NumberUtility.CheckNumber(5, 5);
            Assert.AreEqual(output, 0);
        }
    }
}
