﻿using System;

namespace RRA.ConsoleTests
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string x = "hello-world-hhh-you";

                var CostCenterCodeAndName = x.Split('-');

                string costCenterDescription = string.Empty;
                for (int i = 1; i < CostCenterCodeAndName.Length; i++)
                {
                    if (i == CostCenterCodeAndName.Length - 1)
                    {
                        costCenterDescription += CostCenterCodeAndName[i];
                    }
                    else
                    {
                        costCenterDescription += CostCenterCodeAndName[i]+"-";
                    }
                }

                Console.WriteLine(costCenterDescription);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message.ToString());
            }

        }
    }
}
