﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RRA.ConsoleTests
{
    public static class NumberUtility
    {
        public static int CheckNumber(int numberToCheck, int numberToCheckAgainst)
        {
            int output = 0;
            if (numberToCheckAgainst == numberToCheck)
            {
                output = 0;
            }
            else if(numberToCheckAgainst > numberToCheck)
            {
                output = numberToCheckAgainst - numberToCheck;
            }
            return output;
        }


    }
}
