﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RRA.ETax.Api.Models;
using System.Collections.Generic;

namespace RRA.ETax.Api.Repository
{
    public static class ResponseUtility
    {

        public const string Success = "Request was successfull";
        public const string UnSuccessful = "Request was unsuccessfull";

        internal static ResponseModel CreateResponse<T>(T response, bool isSucessful=true)
        {
            return new ResponseModel
            {
                Message = isSucessful ? Success : UnSuccessful,
                Success = isSucessful,
                Result = !EqualityComparer<T>.Default.Equals(response, default(T)) ? JsonConvert.SerializeObject(response) : null
            };
        }
    }
}
