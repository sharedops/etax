﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RRA.ETax.Repository.Models;
using RRA.ETax.Repository.Contracts;

namespace RRA.ETax.Api.Repository
{
    public class RepositoryBase
    {
        internal ISqlRepository _sqlRepository;
        internal DbConnection _connection { get; set; } = new DbConnection();

        public RepositoryBase(ISqlRepository sqlRepository)
        {
            _sqlRepository = sqlRepository;
            _connection.ConnectionString = "Data Source=saaramsha;Initial Catalog=RRA;Integrated Security=True";
        }

       
    }
}
