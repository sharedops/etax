﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRA.ETax.Api.Models
{
    public class UserRegistrationModel
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        [JsonIgnore]
        public string Password { get; set; }
        public string Address { get; set; }
        public string MobileNumber { get; set; }
        public bool IsActivated { get; set; } = false;
        public Guid ActivationKey { get; set; } = Guid.NewGuid();
    }
}
