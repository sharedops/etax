﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRA.ETax.Api.Models
{
    public class ActivateAccountModel
    {
        public Guid ActivationKey { get; set; }

        public int Id { get; set; }
    }
}
