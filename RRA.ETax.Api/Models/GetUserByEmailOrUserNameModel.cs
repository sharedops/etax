﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRA.ETax.Api.Models
{
    public class GetUserByEmailOrUserNameModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
