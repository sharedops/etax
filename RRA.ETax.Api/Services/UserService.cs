﻿using RRA.ETax.Api.Contracts;
using RRA.ETax.Api.Models;
using RRA.ETax.Api.Repository;
using RRA.ETax.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRA.ETax.Api.Services
{
    public class UserService : RepositoryBase, IUserService
    {
        public UserService(ISqlRepository sqlRepository): base(sqlRepository)
        {

        }
        public async Task<int> CreateUserRegistration(UserRegistrationModel model)
        {
            try
            {
                _connection.StoredProcedure = "spRegisterNewUser";
                _connection.Parameters = model;
                return await _sqlRepository.Execute(_connection);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<GetUserModel> GetUserByUserNameOrEmail(UserRegistrationModel model)
        {
            try
            {
                var user = new GetUserByEmailOrUserNameModel()
                {
                    UserName=model.UserName,
                    Email=model.Email
                };
                _connection.StoredProcedure = "spUserByUserNameOrEmail";
                _connection.Parameters = user;
                return await _sqlRepository.QueryOne<GetUserModel>(_connection);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<int> ActivateAccount(ActivateAccountModel model)
        {
            try
            {
                _connection.StoredProcedure = "spActivateUserAccount";
                _connection.Parameters = model;
                return await _sqlRepository.Execute(_connection);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<IList<GetUserModel>> GetAllUsers()
        {
            try
            {
                _connection.StoredProcedure = "spGetAllUsers";
                return await _sqlRepository.QueryList<GetUserModel>(_connection);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<int> ResetPassword(ResetPasswordModel model)
        {
            try
            {
                _connection.StoredProcedure = "spActivateUserAccount";
                _connection.Parameters = model;
                return await _sqlRepository.Execute(_connection);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
