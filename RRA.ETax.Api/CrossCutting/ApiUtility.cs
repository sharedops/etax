﻿using RRA.ETax.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRA.ETax.Api.CrossCutting
{
    public static class ApiUtility
    {
        public static string getReponseForUserValidation(GetUserModel user, UserRegistrationModel regUser)
        {
            string reposnse = string.Empty;

            if (user.Email == regUser.Email && user.UserName == regUser.UserName && user.IsActivated)
            {
                reposnse = "Account already exist, Please recover your password using forgot password";
            }
            else if (user.UserName == regUser.UserName && user.IsActivated)
            {
                reposnse = "User Name exist, select another user name";
            }
            else if(user.Email == regUser.Email  && user.IsActivated)
            {
                reposnse = "Email exist, Please recover your password using forgot password";
            }
            else if (!user.IsActivated && (user.UserName == regUser.UserName && user.Email==regUser.Email))
            {
                reposnse += "Account not activated, Please activate the account";
            }
            return reposnse;
        }
    }
}
