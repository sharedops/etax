﻿using RRA.ETax.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRA.ETax.Api.Contracts
{
    public interface IUserService
    {
        Task<int> CreateUserRegistration(UserRegistrationModel model);
        Task<GetUserModel> GetUserByUserNameOrEmail(UserRegistrationModel model);
        Task<int> ActivateAccount(ActivateAccountModel model);
        Task<IList<GetUserModel>> GetAllUsers();
        Task<int> ResetPassword(ResetPasswordModel model);
    }
}
