﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RRA.ETax.Api.Contracts;
using RRA.ETax.Api.Repository;
using RRA.ETax.Api.Models;
using Microsoft.AspNetCore.Authorization;
using RRA.ETax.Api.CrossCutting;
using System.Collections.Generic;

namespace RRA.ETax.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController :ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [Authorize]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResponseModel>> RegisterTaxPayer([FromBody]UserRegistrationModel model)
        {
            var user = await _userService.GetUserByUserNameOrEmail(model);
            string resultBody = string.Empty;
            if (user == null)
            {
                int result = await _userService.CreateUserRegistration(model);
                resultBody = result.ToString();
            }
            else
            {
                resultBody = ApiUtility.getReponseForUserValidation(user, model);
            }
            return ResponseUtility.CreateResponse(resultBody);
        }

        [Authorize]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResponseModel>> ActivateUserAcoount([FromBody]ActivateAccountModel model)
        {
            return ResponseUtility.CreateResponse(await _userService.ActivateAccount(model));
        }

        [Authorize]
        [HttpGet("[action]")]
        public async Task<ActionResult<ResponseModel>> GetAllUsers()
        {
            IList<GetUserModel> users = await _userService.GetAllUsers();
            return ResponseUtility.CreateResponse(users);
        }

        [Authorize]
        [HttpPost("[action]")]
        public async Task<ActionResult<ResponseModel>> ResetPassword([FromBody]ResetPasswordModel model)
        {
            return ResponseUtility.CreateResponse(await _userService.ResetPassword(model));
        }

    }
}