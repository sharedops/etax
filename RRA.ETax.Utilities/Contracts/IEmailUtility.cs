﻿
namespace RRA.ETax.Utilities.Contracts
{
    public interface IEmailUtility
    {
        bool SendEmail(string ToAddress, string Body, string Subject);
    }
}
