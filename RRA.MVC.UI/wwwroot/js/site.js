﻿
$(document).ready(function () {
    initSideNav();
    calcHeightOfWindow();
});


function initSideNav() {
    $('.sidenav').sidenav();
}

function calcHeightOfWindow() {
    var windowHeight = $(document).height();
    var mainContainerHeight = windowHeight - 112 + "px";
    console.log(mainContainerHeight);
    $("#body-container").css("height",mainContainerHeight);
}