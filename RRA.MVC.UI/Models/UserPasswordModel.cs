﻿
namespace RRA.MVC.UI.Models
{
    public class UserPasswordModel
    {
        public int UserId { get; set; }
        public string Password { get; set; }
    }
}
