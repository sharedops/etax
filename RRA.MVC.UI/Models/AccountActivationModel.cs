﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RRA.MVC.UI.Models
{
    public class AccountActivationModel
    {
        public int UserId { get; set; }
        public bool IsActivated { get; set; }
        public DateTime AccountActivatedDate { get; set; }
    }
}
